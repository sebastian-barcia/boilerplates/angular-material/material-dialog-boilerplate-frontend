import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

// COMPONENTS
import { DialogCustomComponent } from '../componets/dialog-custom/dialog-custom.component';
import { DialogWithTemplateComponent } from '../componets/dialog-with-template/dialog-with-template.component';
import { DialogCustomData } from '../models/dialog-custom-data';
import { DialogWithTemplateData } from '../models/dialog-with-template-data';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private _matDialog: MatDialog) { }

  openDialogCustom(data: DialogCustomData){
    return this._matDialog.open(DialogCustomComponent, { data, disableClose: true });
  }

  openDialogWithTemplate(data: DialogWithTemplateData) {
    return this._matDialog.open(DialogWithTemplateComponent, { data, disableClose: true });
  }


}
