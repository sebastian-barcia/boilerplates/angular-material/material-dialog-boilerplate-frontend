import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogWithTemplateData } from 'src/app/models/dialog-with-template-data';

@Component({
  selector: 'app-dialog-with-template',
  templateUrl: './dialog-with-template.component.html',
  styleUrls: ['./dialog-with-template.component.scss']
})
export class DialogWithTemplateComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogWithTemplateData) { }

  ngOnInit(): void {
  }

}
