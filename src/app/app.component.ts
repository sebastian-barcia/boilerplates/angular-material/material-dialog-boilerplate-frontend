import { Component, TemplateRef } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { DialogWithTemplateComponent } from './componets/dialog-with-template/dialog-with-template.component';
import { DialogService } from './services/dialog.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'material-dialog-boilerplate-fe';
  private _matDialogRef!: MatDialogRef<DialogWithTemplateComponent>;

  constructor(private _dialogService: DialogService) { }

  ngOnInit(): void {
  }

  openDialogCustom(){
    this._dialogService.openDialogCustom({
      title: 'title 1',
      content: 'content 1'
    }).afterClosed().subscribe( res => console.log('Dialog Custom Closed', res));
  }


  openDialogWithTemplate(template: TemplateRef<any>) {
    this._matDialogRef = this._dialogService.openDialogWithTemplate({
      template
    });

    this._matDialogRef.afterClosed().subscribe(res => console.log('Dialog With template Closed', res) )
  }

  onSave(){
    this._matDialogRef.close();
  }
}
