import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogCustomComponent } from './componets/dialog-custom/dialog-custom.component';

// MATERIAL IMPORTS
import { MatDialogModule } from '@angular/material/dialog';
import { DialogWithTemplateComponent } from './componets/dialog-with-template/dialog-with-template.component';

@NgModule({
  declarations: [
    AppComponent,
    DialogCustomComponent,
    DialogWithTemplateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
